#ifndef __GST_MERGEFIELD_H__
#define __GST_MERGEFIELD_H__

#include <gst/gst.h>
#include <gst/video/video.h>

G_BEGIN_DECLS

/* #defines don't like whitespacey bits */
#define GST_TYPE_MERGEFIELD \
  (gst_merge_field_get_type())
#define GST_MERGEFIELD(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_MERGEFIELD,GstMergeField))
#define GST_MERGEFIELD_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_MERGEFIELD,GstMergeFieldClass))
#define GST_IS_MERGEFIELD(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_MERGEFIELD))
#define GST_IS_MERGEFIELD_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_MERGEFIELD))

typedef struct _GstMergeField      GstMergeField;
typedef struct _GstMergeFieldClass GstMergeFieldClass;

#define MERGE_MAX_BUF 2

struct _GstMergeField
{
  GstElement element;

  GstPad *sinkpad, *srcpad;

  gboolean silent;

  gint32            frate_num, frate_den;
  gint32            width, height;
  gint32            frame_size;
  gboolean          interlaced;
  GstVideoFormat    format;

  GstBuffer         *buf;
  GstCaps           *outcaps;

};

struct _GstMergeFieldClass 
{
  GstElementClass parent_class;
};

GType gst_merge_field_get_type (void);

G_END_DECLS

#endif /* __GST_MERGEFIELD_H__ */
