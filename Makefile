CFLAGS=\
	-DGST_PACKAGE='"GStreamer"' -DGST_ORIGIN='"http://gstreamer.net"' \
	-DVERSION='"0.0"' -DHAVE_USER_MTU -Wall -Wimplicit -g \
	`pkg-config --cflags gstreamer-video-0.10` -I../
	
LDFLAGS=`pkg-config --libs gstreamer-video-0.10`

libgstmergefield.la:gstmergefield.lo
	libtool --mode=link gcc -module -shared -export-symbols-regex gst_plugin_desc $(LDFLAGS) -o $@ $+ -avoid-version -rpath /usr/lib64/gstreamer-0.10/

%.lo: %.c %.h
	libtool --mode=compile gcc $(CFLAGS) -O2 -c -o $@ $<
	
.PHONY: install

install: libgstmergefield.la
	libtool --mode=install install $< /usr/lib64/gstreamer-0.10/

clean:
	rm -rf *.o *.lo *.a *.la .libs