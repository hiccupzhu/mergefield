/**
 * SECTION:element-mergefield
 *
 * FIXME:Describe mergefield here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! mergefield ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>

#include "gdef.h"
#include "gstmergefield.h"

GST_DEBUG_CATEGORY_STATIC (gst_merge_field_debug);
#define GST_CAT_DEFAULT gst_merge_field_debug

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_SILENT
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-raw-yuv")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-raw-yuv")
    );

GST_BOILERPLATE (GstMergeField, gst_merge_field, GstElement, GST_TYPE_ELEMENT);

static void gst_merge_field_finalize(GObject * object);
static void gst_merge_field_set_property (GObject * object, guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_merge_field_get_property (GObject * object, guint prop_id, GValue * value, GParamSpec * pspec);

static gboolean gst_merge_field_set_caps (GstPad * pad, GstCaps * caps);
static GstFlowReturn gst_merge_field_chain (GstPad * pad, GstBuffer * buf);
static GstCaps *     gst_merge_field_src_getcaps (GstMergeField * filter);

/* GObject vmethod implementations */

static void
gst_merge_field_base_init (gpointer gclass)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (gclass);

  gst_element_class_set_details_simple(element_class,
    "MergeField",
    "FIXME:Generic",
    "FIXME:Generic Template Element",
    "szhu <<user@hostname.org>>");

  gst_element_class_add_pad_template (element_class, gst_static_pad_template_get (&src_factory));
  gst_element_class_add_pad_template (element_class, gst_static_pad_template_get (&sink_factory));
}

/* initialize the mergefield's class */
static void
gst_merge_field_class_init (GstMergeFieldClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gobject_class->finalize = gst_merge_field_finalize;
  gobject_class->set_property = gst_merge_field_set_property;
  gobject_class->get_property = gst_merge_field_get_property;

  g_object_class_install_property (gobject_class, PROP_SILENT,
      g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
          FALSE, G_PARAM_READWRITE));
}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_merge_field_init (GstMergeField * filter,
    GstMergeFieldClass * gclass)
{
    int i;

    filter->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
    gst_pad_set_setcaps_function (filter->sinkpad, GST_DEBUG_FUNCPTR(gst_merge_field_set_caps));
    //  gst_pad_set_getcaps_function (filter->sinkpad, GST_DEBUG_FUNCPTR(gst_pad_proxy_getcaps));
    gst_pad_set_chain_function (filter->sinkpad, GST_DEBUG_FUNCPTR(gst_merge_field_chain));

    filter->srcpad = gst_pad_new_from_static_template (&src_factory, "src");
//    gst_pad_set_getcaps_function (filter->srcpad, GST_DEBUG_FUNCPTR(gst_merge_field_src_getcaps));

    gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);
    gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad);


    filter->silent = FALSE;
    filter->buf = NULL;
    filter->outcaps = NULL;
}

static void
gst_merge_field_finalize(GObject * object)
{
    GstMergeField * filter = (GstMergeField *)object;

    if(filter->outcaps){
        gst_caps_unref(filter->outcaps);
        filter->outcaps = NULL;
    }
    if(filter->buf){
        gst_buffer_unref(filter->buf);
        filter->buf = NULL;
    }
}

static void
gst_merge_field_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstMergeField *filter = GST_MERGEFIELD (object);

  switch (prop_id) {
    case PROP_SILENT:
      filter->silent = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_merge_field_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstMergeField *filter = GST_MERGEFIELD (object);

  switch (prop_id) {
    case PROP_SILENT:
      g_value_set_boolean (value, filter->silent);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static GstCaps *
gst_merge_field_src_getcaps (GstMergeField * filter)
{
    GstCaps *caps;

    caps = filter->outcaps;

    if(caps == NULL){
        caps = gst_caps_new_simple ("video/x-raw-yuv",
                "format",     GST_TYPE_FOURCC,  gst_video_format_to_fourcc(filter->format),
                "interlaced", G_TYPE_BOOLEAN,   filter->interlaced,
                "framerate",  GST_TYPE_FRACTION, filter->frate_num / 2, filter->frate_den,
                "width",      G_TYPE_INT, filter->width,
                "height",     G_TYPE_INT, filter->height * 2,
                NULL);
        filter->outcaps = caps;
        av_print("caps=%s\n", gst_caps_to_string(caps));
    }

    gst_caps_ref(caps);

    return caps;
}



static gboolean
gst_merge_field_set_caps (GstPad * pad, GstCaps * caps)
{
    gboolean ret;
    GstMergeField *filter;
    GstStructure  *s;
    guint32 fourcc;

    filter = GST_MERGEFIELD (gst_pad_get_parent (pad));

    av_print("caps=%s\n", gst_caps_to_string(caps));

    s = gst_caps_get_structure(caps, 0);

    gst_structure_get_fraction(s, "framerate", &filter->frate_num, &filter->frate_den);
    gst_structure_get_fourcc  (s, "format", &fourcc);
    gst_structure_get_int     (s, "width", &filter->width);
    gst_structure_get_int     (s, "height", &filter->height);
    gst_structure_get_boolean (s, "interlaced", &filter->interlaced);

    filter->format = gst_video_format_from_fourcc(fourcc);

    filter->frame_size = gst_video_format_get_size(filter->format, filter->width, filter->height);

    ret = gst_pad_set_caps (filter->srcpad, gst_merge_field_src_getcaps(filter));
    gst_object_unref (filter);

    return ret;
}

static void
gst_merge_field_copy_buffer(GstMergeField * filter, GstBuffer * buf0, GstBuffer * buf1, GstBuffer * buf2){
#define MEMCPY(dst, src, len) \
    {\
    memcpy(dst, src, len);\
    dst += len;\
    src += len;\
    }

    guint8 *p0, *p1, *p2;
    gint32 i, j, stride, height;

    stride = GST_ROUND_UP_4(filter->width * 2);
    height = GST_ROUND_UP_2(filter->height);

    p0 = GST_BUFFER_DATA(buf0);
    p1 = GST_BUFFER_DATA(buf1);
    p2 = GST_BUFFER_DATA(buf2);

    for(i = 0; i < height; i++){
        MEMCPY(p0, p1, stride);
        MEMCPY(p0, p2, stride);
    }
}

static GstFlowReturn
gst_merge_field_chain (GstPad * pad, GstBuffer * buf)
{
    GstMergeField *filter;
    GstBuffer     *outbuf;
    GstCaps       *caps;

    filter = GST_MERGEFIELD (GST_OBJECT_PARENT (pad));

    if (filter->silent == TRUE){
        return gst_pad_push (filter->srcpad, buf);
    }

    if(filter->buf == NULL){
        if(GST_BUFFER_FLAG_IS_SET(buf, GST_VIDEO_BUFFER_TFF)){
            filter->buf = buf;
        }else{
            av_printf("Drop UNKNOWN field.\n");
            gst_buffer_unref(buf);
        }
        return GST_FLOW_OK;
    }

    outbuf = gst_buffer_new_and_alloc(filter->frame_size * 2);

    gst_merge_field_copy_buffer(filter, outbuf, filter->buf, buf);
    GST_BUFFER_TIMESTAMP(outbuf) = GST_BUFFER_TIMESTAMP(filter->buf);

    caps = gst_merge_field_src_getcaps(filter);
    gst_buffer_set_caps(outbuf, caps);

    gst_buffer_unref(filter->buf);
    filter->buf = NULL;
    gst_buffer_unref(buf);

    return gst_pad_push (filter->srcpad, outbuf);
}


/* entry point to initialize the plug-in
 * initialize the plug-in itself
 * register the element factories and other features
 */
static gboolean
mergefield_init (GstPlugin * mergefield)
{
  /* debug category for fltering log messages
   *
   * exchange the string 'Template mergefield' with your description
   */
  GST_DEBUG_CATEGORY_INIT (gst_merge_field_debug, "mergefield",
      0, "Template mergefield");

  return gst_element_register (mergefield, "mergefield", GST_RANK_NONE,
      GST_TYPE_MERGEFIELD);
}

/* PACKAGE: this is usually set by autotools depending on some _INIT macro
 * in configure.ac and then written into and defined in config.h, but we can
 * just set it ourselves here in case someone doesn't use autotools to
 * compile this code. GST_PLUGIN_DEFINE needs PACKAGE to be defined.
 */
#ifndef PACKAGE
#define PACKAGE "myfirstmergefield"
#endif

/* gstreamer looks for this structure to register mergefields
 *
 * exchange the string 'Template mergefield' with your mergefield description
 */
GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    "mergefield",
    "Template mergefield",
    mergefield_init,
    VERSION,
    "LGPL",
    "GStreamer",
    "http://gstreamer.net/"
)
